target extended-remote /dev/ttyBmpGdb

monitor tpwr enable
shell sleep 1
monitor swdp_scan
attach 1

# print demangled symbols
set print asm-demangle on

# set backtrace limit to not have infinite backtrace loops
set backtrace limit 32

# detect unhandled exceptions, hard faults and panics
break DefaultHandler
break HardFault
break rust_begin_unwind
break main

load

continue
continue