#![deny(unsafe_code)]
// #![deny(warnings)]
#![no_main]
#![no_std]

extern crate heapless;
extern crate nb;
extern crate panic_semihosting;

use heapless::consts::*;
use heapless::Vec;
use nb::block;

use cortex_m_semihosting::{debug, hprint, hprintln};

use rtfm::export::SystClkSource;
use stm32f0xx_hal::{
    delay::Delay,
    gpio,
    gpio::gpioa::{PA0, PA1, PA2},
    prelude::*,
    serial,
    serial::Rx,
    serial::Serial,
    stm32::TIM3,
    stm32::USART1,
    time::Hertz,
    timers,
    timers::Timer,
};

pub struct LedRgb {
    red: PA1<gpio::Output<gpio::PushPull>>,
    green: PA2<gpio::Output<gpio::PushPull>>,
    blue: PA0<gpio::Output<gpio::PushPull>>,
}

#[derive(Clone)]
pub struct Color {
    red: u8,
    green: u8,
    blue: u8,
}

impl Color {
    const fn new(r: u8, g: u8, b: u8) -> Color {
        Color {
            red: r,
            green: g,
            blue: b,
        }
    }

    fn set_color(&mut self, r: u8, g: u8, b: u8) {
        self.red = r;
        self.green = g;
        self.blue = b;
    }
}

#[rtfm::app(device = stm32f0::stm32f0x0, peripherals = true)]
const APP: () = {
    struct Resources {
        #[init(Color::new(0, 0, 0))]
        color: Color,
        #[init(Vec(heapless::i::Vec::new()))]
        buffer: heapless::Vec<u8, U513>,
        led: LedRgb,
        delay: Delay,
        timer: Timer<TIM3>,
        rx: Rx<USART1>,
    }

    #[init]
    fn init(cx: init::Context) -> init::LateResources {
        // Cortex-M peripherals
        let core: cortex_m::Peripherals = cx.core;

        // Device specific peripherals
        let device: stm32f0::stm32f0x0::Peripherals = cx.device;

        /* Constrain clocking registers */
        let rcc = device.RCC.constrain();

        /* Configure clock to 8 MHz (i.e. the default) and freeze it */
        let clocks = rcc
            .cfgr
            .sysclk(48.mhz())
            .hclk(48.mhz())
            .pclk(24.mhz())
            .freeze();

        let mut syst = core.SYST;

        syst.set_clock_source(SystClkSource::Core);
        syst.set_reload(48_000_000); // period = 1s
        syst.enable_counter();

        let delay = Delay::new(syst, clocks);

        hprintln!("initializing").unwrap();

        let gpioa = device.GPIOA.split();
        let led = LedRgb {
            red: gpioa.pa1.into_push_pull_output(),
            green: gpioa.pa2.into_push_pull_output(),
            blue: gpioa.pa0.into_push_pull_output(),
        };

        let mut timer = Timer::tim3(device.TIM3, Hertz(10000), clocks);
        timer.listen(timers::Event::TimeOut);

        let tx = gpioa.pa9.into_alternate_af1();
        let rx = gpioa.pa3.into_alternate_af1();
        let mut serial = Serial::usart1(device.USART1, (tx, rx), 115_200.bps(), clocks);
        serial.listen(serial::Event::Rxne);
        let (_, rx) = serial.split();

        hprintln!("intialized").unwrap();

        debug::exit(debug::EXIT_SUCCESS);
        init::LateResources {
            led,
            delay,
            timer,
            rx,
        }
    }

    #[task(binds = USART1, priority = 2, resources = [rx, buffer, color])]
    fn usart1(cx: usart1::Context) {
        let rx: &mut Rx<USART1> = cx.resources.rx;
        let buffer: &mut heapless::Vec<u8, U513> = cx.resources.buffer;
        buffer.push(rx.read().unwrap());
        if buffer.len() >= 8 {
            match buffer[0] as char {
                'a' => cx.resources.color.red = 32,
                'b' => cx.resources.color.red = 255,
                'c' => cx.resources.color.red = 0,
                _ => (),
            }
            hprintln!("{:?}", buffer[0]);
            buffer.clear();
        }
    }

    #[task(binds = TIM3, priority = 1, resources = [timer, color, led])]
    fn tim3(mut cx: tim3::Context) {
        static mut COUNT: u8 = 0;
        static mut COLOR: Color = Color::new(0, 0, 0);
        let led: &mut LedRgb = cx.resources.led;

        if *COUNT == 0u8 {
            cx.resources.color.lock(|color| *COLOR = color.clone());
            if COLOR.red != 0u8 {
                led.red.set_high();
            }
            if COLOR.green != 0u8 {
                led.green.set_high();
            }
            if COLOR.blue != 0u8 {
                led.blue.set_high();
            }
        } else if COLOR.red == *COUNT {
            led.red.set_low();
        } else if COLOR.green == *COUNT {
            led.green.set_low();
        } else if COLOR.blue == *COUNT {
            led.blue.set_low();
        }

        *COUNT += 1;
        let _ = cx.resources.timer.wait();
    }

    #[idle(resources = [color, led, delay, rx])]
    fn idle(mut cx: idle::Context) -> ! {
        let delay: &mut Delay = cx.resources.delay;
        loop {
            hprintln!("idling").unwrap();
            delay.delay_ms(2000_u16);
        }
    }
};
